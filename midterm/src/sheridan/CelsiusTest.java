package sheridan;

/*
 * Yi Tan
 * 991554680
 */

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.Assert;


public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int cel = Celsius.fromFahrenheit(68);
		assertTrue("The cel provided doesnot match the result", cel==20);
	}
	
	

	
	@Test 
	public void testFromFahrenheitBoundaryIn() {
		int cel = Celsius.fromFahrenheit(99);
		assertTrue("The cel provided doesnot match the result", cel==37);
	}
	
	@Test 
	public void testFromFahrenheitBoundaryOut() {
		int cel = Celsius.fromFahrenheit(98);
		assertFalse("The cel provided doesnot match the result", cel==37);
	}
	

}
